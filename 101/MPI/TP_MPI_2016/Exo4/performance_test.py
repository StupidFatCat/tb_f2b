import signal
import sys
import os
import time

#Predefined
hostlist = ["srv-disi-vnc-01",\
        #   "srv-disi-vnc-02",\
            "srv-disi-vnc-04",\
            "srv-disi-vnc-06",\
            "pc-df-207",\
            "pc-df-208",\
            "pc-df-210",\
            "pc-df-211",\
            ]
output_file = "performance_output.csv"
output_folder = "output"
npernode    = 4
hostnum     = 1
sleep_time  = 1
host =      ""
task_col_num = 4

test_id = 0
test_script = []


current_timestamp = lambda: int(round(time.time()* 1000))

def singal_handler(signal, frame):
    print "You pressed Ctrl+C!\n"
    sys.exit(0)

signal.signal(signal.SIGINT, singal_handler)

def main():
    testlist = ["0", "1", "2"]
    if len(sys.argv) > 1:
       testlist = sys.argv[1].split(',')

    for key in testlist:
        launch_test(key)



def launch_test(key):
    { "0" : task_col_num_test,
      "1" : npernode_test,
      "2" : hostnum_test,
      "3" : sleep_time_test
    }[key]()

# Scenarios 0
def task_col_num_test():
    print "[task_col_num_test]\n"
    global npernode, hostnum, task_col_num, output_file, test_id
    test_id = 0
    npernode = 4
    hostnum = 1
    loop = 10

    output_file = "task_col_num_test_" + str(current_timestamp()) + ".csv"

    add_header()
    for task_col_num in my_generator(1, 500, 5):
        call_script(loop)

# Scenarios 1
def npernode_test():
    print "[npernode_test]\n"
    global npernode, hostnum, task_col_num, output_file, test_id
    test_id = 1
    hostnum = 1
    task_col_num = 4
    loop = 10

    output_file = "npernode_test_" + str(current_timestamp()) + ".csv"

    add_header()
    for npernode in my_generator(2, 8, 1):
        call_script(loop)


# Scenarios 2
def hostnum_test():
    print "[hostnum_test]\n"
    global npernode, hostnum, task_col_num, output_file, test_id
    test_id = 2
    task_col_num = 4
    loop = 10

    output_file = "hostnum_test_" + str(current_timestamp()) + ".csv"

    add_header()
    for hostnum in my_generator(2, 7, 1):
        call_script(loop)

# Scenarios 3
def sleep_time_test():
    print "[sleep_time_test]\n"
    print "In order to test sleep_time_test, you should do 'make clean;"\
            "make sleep'\n"
    global npernode, hostnum, task_col_num, sleep_time, output_file, test_id
    test_id = 3
    npernode = 4
    task_col_num = 40
    loop = 10

    output_file = "sleep_time_test_" + str(current_timestamp()) + ".csv"

    add_header()
    for sleep_time in my_generator(1, 4, 1):
        call_script(loop)


def my_generator(start, end, step):
    while start <= end:
        yield start
        start += step

def add_header():
    ## slave_num , task_col_num, sleep_time, costed_time
    os.system('echo "slave_num;task_col_num;sleep_time;costed_time;\n"' \
               " >> " +  output_folder + "/" + output_file)

def update_script():
    global test_script, host
    host = ','.join(hostlist[:hostnum])

    test_script = {
                  #0 : ["mpirun", "--npernode", str(npernode),\
                  # "mandel-basic-mpi", str(task_col_num)],
                  0 : ["mpirun", "--host", "pc-df-201,pc-df-202,pc-df-203,pc-df-204",\
                   "mandel-basic-mpi", str(task_col_num)],

                  1 : ["mpirun", "--npernode", str(npernode),\
                   "mandel-basic-mpi", str(task_col_num)],

                  2 : ["mpirun", "--host", host,\
                   "mandel-basic-mpi", str(task_col_num)],

                  3 : ["mpirun", "--npernode", str(npernode),\
                   "mandel-basic-mpi", str(task_col_num), str(sleep_time)]
    }[test_id]

    test_script.extend([">>", output_folder + "/" + output_file])

def call_script(loop = 1):
    update_script()
    tmp_script = ' '.join(test_script)
    print "script: ", tmp_script , "\n"
    for x in range(1, loop):
        os.system(tmp_script)



if __name__ == "__main__":
    main()
