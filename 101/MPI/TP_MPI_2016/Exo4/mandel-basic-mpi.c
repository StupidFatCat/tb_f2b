#include <stdio.h>
#include "mpi.h"

#if defined(__GNUC__) && (__GNUC__ >= 3)
#define ATTRIBUTE(x) __attribute__(x)
#else
#define ATTRIBUTE(x) /**/
#endif

#define MIN(_x, _y) ((_x) > (_y) ? (_y) : (_x))
#define ABS(_x) ((_x) >= 0 ? (_x) : -(_x))


/* N'hesitez pas a changer MAXX .*/
#define MAXX  500
#define MAXY (MAXX * 3 / 4)

#define NX (2 * MAXX + 1)
#define NY (2 * MAXY + 1)

#define NBITER 550
#define DATATAG 150

//#define TASK_COL_NUMBER 3
//#define TASK_BUFFER_LENGTH  (TASK_COL_NUMBER * NY)

int TASK_COL_NUMBER = 3;
int SLEEP_TIME = 0;
int TASK_BUFFER_LENGTH;

static int mandel(double, double);

int dump_ppm(const char *, int[NX][NY]);
int cases[NX][NY];

int fill_in_cases(int image_recv[]);

int main(int argc, char *argv[])
{
    // Default task_col_number
    if (argc >= 2){
        TASK_COL_NUMBER = atoi(argv[1]);
    } if (argc == 3) {
        SLEEP_TIME = atoi(argv[2]);
    }
    TASK_BUFFER_LENGTH = (TASK_COL_NUMBER * NY);

    MPI_Status status;
    int i,j, num, rank, size, nbslaves;
    char inputstr [100],outstr [100];




  /* Start up MPI */

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  nbslaves = size -1;
  if (nbslaves <= 0) {
      fprintf(stderr, "Need more than 1 slave.");
      exit(-1);
  }

  if (rank == 0) {
    int image_recv[TASK_BUFFER_LENGTH + 1];
    double time_start, time_end;

    //printf("slaves num: %d\n", nbslaves);
    //printf("TASK_COL_NUMBER : %d\n", TASK_COL_NUMBER);

    /* Begin User Program  - the master */
    time_start = MPI_Wtime();
    int next_col;
    next_col = 0;

    for (i = 1; i <= nbslaves; ++i) {
        MPI_Send(&next_col, 1, MPI_INT, i, DATATAG, MPI_COMM_WORLD);
        next_col += TASK_COL_NUMBER;
        if (next_col >= NX) {
            break;
        }
    }

    int num_col_recv = 0;

    while (num_col_recv < NX) {
        MPI_Recv(&image_recv, sizeof(image_recv)/sizeof(int), MPI_INT, MPI_ANY_SOURCE, DATATAG, MPI_COMM_WORLD, &status);

        if (next_col < NX) {
            MPI_Send(&next_col, 1, MPI_INT, status.MPI_SOURCE, DATATAG, MPI_COMM_WORLD);
            next_col += TASK_COL_NUMBER;
        }

        fill_in_cases(image_recv);
        num_col_recv += TASK_COL_NUMBER;
    }

    dump_ppm("mandel.ppm", cases);
    time_end = MPI_Wtime();
    //printf("Fini.\n");
    //printf("Costed time: %1.3f\ns", time_end -time_start);
    
    //printf for performance_test
    // slave_num , task_col_num, sleep_time, costed_time
    printf("%d;%d;%d;%1.3f\n", nbslaves, TASK_COL_NUMBER, SLEEP_TIME, time_end - time_start);


    // send a "shutdowm" message to slaves
    for(i=1 ; i <= nbslaves; ++i) {
      MPI_Send(&next_col, 0, MPI_INT, i, DATATAG, MPI_COMM_WORLD); 
    }
  } else {

    /* On est l'un des fils */
    double x, y;
    int res[TASK_BUFFER_LENGTH + 1];
    int res_index;
    int i, j, rc;

    int receivedsize;
    int col_index;

    int notover = 1;

    while (notover) {
        MPI_Recv(&col_index, 1, MPI_INT, 0, DATATAG, MPI_COMM_WORLD, &status);

        /* we call MPI_Get_count to determine the size of the message received */
        MPI_Get_count(&status, MPI_INT, &receivedsize);
        if (receivedsize == 0) {
            notover = 0;
            //printf("slave %d quit\n", rank);
        } else {
            res_index = 0;
            for(i = col_index - MAXX; i <= MIN(col_index+TASK_COL_NUMBER-MAXX-1, MAXX); ++i) {
                for(j = -MAXY; j <= MAXY; j++) {
                    x = 2 * i / (double)MAXX;
                    y = 1.5 * j / (double)MAXY;

                    res[res_index++] = mandel(x, y);
                }
            }
#ifdef SLEEP_TEST
                    sleep(SLEEP_TIME);
#endif

            res[TASK_BUFFER_LENGTH] = col_index;
            MPI_Send(res, sizeof(res)/sizeof(int), MPI_INT, 0, DATATAG, MPI_COMM_WORLD); 
        }
    }

}
  MPI_Finalize();
  return 0;
}



/* function to compute a point - the number of iterations 
   plays a central role here */

int
mandel(double cx, double cy)
{
  int i;
  double zx, zy;
  zx = 0.0; zy = 0.0;
  for(i = 0; i < NBITER; i++) {
    double zxx = zx, zyy = zy;
    zx = zxx * zxx - zyy * zyy + cx;
    zy = 2 * zxx * zyy + cy;
    if(zx * zx + zy * zy > 4.0)
      return i;
  }
  return -1;
}

/* the image commputer can be transformed in a ppm file so
   to be seen with xv */

int
dump_ppm(const char *filename, int valeurs[NX][NY])
{
  FILE *f;
  int i, j, rc;

  f = fopen(filename, "w");
  if(f == NULL) { perror("fopen"); exit(1); }
  fprintf(f, "P6\n");
  fprintf(f, "%d %d\n", NX, NY);
  fprintf(f, "%d\n", 255);
  for(j = NY - 1; j >= 0; j--) {
    for(i = 0; i < NX; i++) {
      unsigned char pixel[3];
      if(valeurs[i][j] < 0) {
	pixel[0] = pixel[1] = pixel[2] = 0;
      } else {
	unsigned char val = MIN(valeurs[i][j] * 12, 255);
	pixel[0] = val;
	pixel[1] = 0;
	pixel[2] = 255 - val;
      }
      rc = fwrite(pixel, 1, 3, f);
      if(rc < 0) { perror("fwrite"); exit(1); }
    }
  }
  fclose(f);
  return 0;
}

int fill_in_cases(int image_recv[]) {
    int col_index = image_recv[TASK_BUFFER_LENGTH];
    int j;
    int delta_col, delta_row;
    int real_buffer_length;

    if ((col_index + TASK_COL_NUMBER) >= NX) {
        real_buffer_length = (NX - col_index) * NY; 
    } else {
        real_buffer_length = TASK_BUFFER_LENGTH;
    }
    
    for (j = 0; j < real_buffer_length; ++j) {
        delta_col = j/NX;
        delta_row = j%NY;
        cases[col_index + delta_col][delta_row] = image_recv[j];
    }

    return 1;
}
 
