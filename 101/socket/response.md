### TP Socket F2B

# 1er partie (UDP)

1. Sender send "text" then quit 
   Receiver receive "text" and print it to stdout then quit

2. Sender sent "text" before Receiver create the socket then quit, 
and Receiver is always waiting and receive nothing.

3. After changing host address to `REMOTE_HOST`, we can observe the same result as 1st.

4. Receiver can only receive the first 9 characters, because the buffer length is 10.

5. Receiver throws an `SocketTimeoutException` after 5 seconds. 

6. receiver can receive message from any sender using the socket and do not quit.

# 2eme partie (TCP)

1. Requestor connected to Acceptor and receive the text "titi",
Acceptor accept the connection request and sent "titi" to Requestor.

2. Requestor throws an `ConnectException: Connection refused` because there is no Acceptor.

3. Add another `arg` to Acceptor.

4. Concerning Example5, MyStreamSocket wrapped `Socket` and `InputStream`/ `OutputStream` together. It's more convenient for developper to use the Socket only for send/receive messages.

# 3eme partie (chat)

1. SenderReceiver and ReceiverSender both created their own socket. SenderReceiver sent msg1 to ReceiverSender firstly and then ReceiverSender sent msg2 to SenderReceiver.
The order is important because `ReceiverSender` set itself to `Receive` msg firstly, othervise `ReceiverSender` can not `Receive` msg and can not pass itself to the status of send.

2. 2 programs search the filename in current dir.
2 programs search file in the same location. a database, or a file server.


