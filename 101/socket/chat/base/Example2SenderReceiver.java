import java.io.*;
import java.net.*;
import java.lang.*;

/**
 * This example illustrates a process which sends then receives
 * using a datagram socket.
 * @author M. L. Liu
 */
public class Example2SenderReceiver {
// An application which sends then receives a message using
// connectionless datagram socket.
// Four command line arguments are expected, in order: 
//    <domain name or IP address of the receiver>
//    <port number of the receiver's datagram socket>
//    <port number of this process's datagram socket>
//    <message, a string, to send>

   public static void main(String[] args) {
      if (args.length != 4)
         System.out.println
            ("This program requires four command line arguments");
      else {
        String machine = "";
        String port = "";
        // write host port
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(args[0]));
            InetAddress addr = InetAddress.getLocalHost();
            String hostname = addr.getHostName();
            out.write(hostname + "\n");
            out.write(args[1]);
            out.close();
        } catch (IOException e) {
        }
        // read host port
        try {
            Thread.sleep(10000);
            BufferedReader in = new BufferedReader(new FileReader(args[2]));
            machine = in.readLine();
            port = in.readLine();

            System.out.println("Send/Receive to/from :");
            System.out.println(machine);
            System.out.println(port);
            in.close();
        } catch (Exception e) {
            e.printStackTrace( );
        }
        // do job
         try {      
  		     
            InetAddress receiverHost = InetAddress.getByName(machine);
            int receiverPort = Integer.parseInt(port);
            int myPort = Integer.parseInt(args[1]);
            String message = args[3];
   	      MyDatagramSocket mySocket = new MyDatagramSocket(myPort);  
               // instantiates a datagram socket for both sending
               // and receiving data
          int loop = 1000;
          for (; loop > 0; --loop) {
            mySocket.sendMessage( receiverHost, receiverPort, message);
				// now wait to receive a datagram from the socket
            System.out.println(mySocket.receiveMessage());
            Thread.sleep(3000);
          }
				mySocket.close( );
         } // end try
	      catch (Exception ex) {
            ex.printStackTrace( );
	      } //end catch
      } //end else
   } //end main 

} //end class
