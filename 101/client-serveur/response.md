# TP client-server

## 1 partie

1. ......

2. if run client firstly, the request sent by client is lost. so the client is always waiting for the response from server. if run server firstly, they are worked.

3. In order to encapsulate functions. So the other classes can also  use it easily. 

4. Because the server should reply on sender, so server need to know the the address and port of client.


## 2 partie

1. client and server are using `StreamSocket` instead of `DatagramSocket`. For example, the server wait to accept a connection instead of waiting a request, and the server send the timestamp through the connection directly without knowing the address or port of client.

2. 2 clients receive the response correctly and 2 clients received with a delay of 3 sec. Because of the TCP connection,  the request won't lose, so does the response. The server replied 1 request then slept 3 secs.

## 3 partie

1. ...
2. ...

3. The server should be activated in order to provide service anytime.

4. The `Helper` encapsulates the `StreamSocket` , send and receive functions. 
