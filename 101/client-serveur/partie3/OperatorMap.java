import java.util.*;

public class OperatorMap {
	private HashMap<String, Operator> map;


	public class AddOperator implements Operator {
		public String calcul(Integer num1, Integer num2) {
			return new Integer(num1 + num2).toString();
		}
	}

	public class MulOperator implements Operator {
		public String calcul(Integer num1, Integer num2) {
			return new Integer(num1 * num2).toString();
		}
	}

	public OperatorMap() {
		map = new HashMap<String, Operator>();
		map.put("ADD", new AddOperator());
		map.put("MUL", new AddOperator());
	}

	public String execute(String optStr, Integer num1, Integer num2) {
		Operator op = map.get(optStr);

		if (op != null) {
			return op.calcul(num1, num2);
		} else {
			return "Operator Error: " + optStr;
		}
	}

}
