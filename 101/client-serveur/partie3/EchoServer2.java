import java.io.*;
import java.net.*;

/**
 * This module contains the application logic of an echo server
 * which uses a stream socket for interprocess  communication.
 * A command-line argument is required to specify the server port.
 * @author M. L. Liu
 */
public class EchoServer2 {
   static final String endMessage = ".";

   public static String calculate(OperatorMap om, String message) {
      String[] ops = message.split("\\W");
      if (ops.length != 3) {
         return "Error Operation! length :" + ops.length;
      }
      try {
         Integer opt1 = Integer.parseInt(ops[1]);
         Integer opt2 = Integer.parseInt(ops[2]);
		 return om.execute(ops[0], opt1, opt2);

      } catch (Exception ex) {
         ex.printStackTrace();
      }

      return "Error Operation! '" + ops[0] + "'";
   }

   public static void main(String[] args) {
      int serverPort = 7;    // default port
      String message;

      if (args.length == 1 )
         serverPort = Integer.parseInt(args[0]);       
      try {
         // instantiates a stream socket for accepting
         //   connections
	   OperatorMap om = new OperatorMap();

   	   ServerSocket myConnectionSocket = 
            new ServerSocket(serverPort); 
/**/     System.out.println("Daytime server ready.");  
         while (true) {  // forever loop
            // wait to accept a connection 
/**/        System.out.println("Waiting for a connection.");
            MyStreamSocket myDataSocket = new MyStreamSocket
                (myConnectionSocket.accept( ));
/**/        System.out.println("connection accepted");
            boolean done = false;
            while (!done) {
               message = myDataSocket.receiveMessage( );
/**/           System.out.println("message received: "+ message);
               if ((message.trim()).equals (endMessage)){
                  //Session over; close the data socket.
/**/              System.out.println("Session over.");
                  myDataSocket.close( );
                  done = true;
               } //end if
               else {
                  // Now send the echo to the requestor
				  
                  myDataSocket.sendMessage(calculate(om, message));
               } //end else
		       } //end while !done
         } //end while forever
       } // end try
	    catch (Exception ex) {
          ex.printStackTrace( );
	    }
   } //end main
} // end class
