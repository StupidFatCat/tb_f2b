#ifndef _ENVOIE_FICHIER_H_
#define _ENVOIE_FICHIER_H_
#include <stdlib.h>

ssize_t envoie_fichier(int out_fd, int in_fd, size_t count);

#endif
