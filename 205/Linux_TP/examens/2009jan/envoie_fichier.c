#include <stdlib.h>
#include "envoie_fichier.h"

ssize_t envoie_fichier(int out_fd, int in_fd, size_t count) {
    char buf[count];
    ssize_t bytes_read = read(in_fd, buf, count);
    if (bytes_read < 0) {
        perror("read");
        return -1;
    }

    ssize_t bytes_write = write(out_fd, buf, count);
    if (bytes_write < 0) {
        perror("write");
        return -1;
    }

    return bytes_write;
}

