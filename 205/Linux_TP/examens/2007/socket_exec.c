#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int my_socketpair(int domain, int type, int protocol, int sv[2]) {
    sv[0] = socket(domain, type, protocol);
    sv[1] = socket(domain, type, protocol);
    
    if (type i== SOCK_STREAM) {
        sv[0];
        // todo
    }
    
}

int socket_exec(const char* command) {
    //Creation of two sockets
    int sockets[2], pid;
    char buf[1024];

    if (socketpair(PF_LOCAL, SOCK_STREAM, 0, sockets) < 0) {
        perror("opening stream socket pair");
        return -1;
    }

    if ((pid = fork()) == -1)
        perror("fork");

    // Parent
    else if (pid) {
        close(sockets[0]);
        return sockets[1];

    // Child
    } else {
        close(sockets[1]);

        //dup to stdin, stdout
        close(0);
        close(1);
        dup(sockets[0]);
        dup(sockets[0]);
        close(sockets[0]);

        execlp("/bin/sh", "sh", "-c", command);
        perror("execlp");
    }
}

#define MSG "coucou\ntout le monde\n"

int main() {
    char buff[200];
    ssize_t sz;

    int se = socket_exec("wc");

    strcpy(buff, MSG);
    write(se, buff, strlen(MSG) + 1);

    shutdown(se, SHUT_WR);

    sz = read(se, buff, 200);
    buff[sz] = '\0';
    printf("Result %s", buff);

    wait(0);
    exit(EXIT_SUCCESS);
}

