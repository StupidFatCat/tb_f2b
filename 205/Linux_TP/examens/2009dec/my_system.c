#include <stdlib.h>
#include <signal.h>
#include "my_system.h"

int my_system(const char *command) {
    int pid = fork();
    int *status;
    int res;
    signal(SIGINT, SIG_IGN);
    signal(SIGQUIT, SIG_IGN);
    if (pid < 0) {
        perror("fork");
        return -1;

    // Child
    } else if (pid == 0) {
        res = execlp("/bin/sh", "sh", "-c", command);
        if(res < 0) {
            exit(127);
        } else {
            exit(res);
        }

    // Parent
    } else {
        wait(status);
        return WEXITSTATUS(status);
    }
}
