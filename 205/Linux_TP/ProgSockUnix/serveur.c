#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/un.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <unistd.h>

#define BUFSIZE 512

int main(int argc, char **argv) {
    int sfd, s, ns, r;
    struct addrinfo hints;
    struct addrinfo *result, *rp;
    struct sockaddr_un addr;
    char buf[BUFSIZE];
    ssize_t nread, nwrite;
    struct sockaddr_storage from;
    socklen_t fromlen;
    char* socket_path = "mysocket";
    char host[NI_MAXHOST];
    char *message = "Message a envoyer: ";

    printf("Usage: %s  socket_path(mysocket as default)\n", argv[0]);

    if (argc > 1) {
        socket_path = argv[1];
    }

    // ---------------------------
    sfd = socket(PF_UNIX, SOCK_SEQPACKET, 0);
    if (sfd == -1) {
        perror("socket error");
        exit(EXIT_FAILURE);
    }
    memset(&addr, 0, sizeof(addr));
    addr.sun_family = PF_UNIX;
    strncpy(addr.sun_path, socket_path, sizeof(addr.sun_path) - 1);
    r = bind(sfd, (struct sockaddr *)&addr, sizeof(addr));
    if (r == -1) {
        close(sfd);
        perror("bind");
        exit(EXIT_FAILURE);
    }

    /* Positionnement de la machine a etats TCP sur listen */
    listen(sfd, 5);

    for (;;) {
        /* Acceptation de connexions */
        fromlen = sizeof(from);
        ns = accept(sfd, NULL, NULL);
        if (ns == -1) {
            perror("accept");
            exit(EXIT_FAILURE);
        }


        for (;;) {
            nwrite = write(ns, message, strlen(message));
            if (nwrite < 0) {
                perror("write");
                close(ns);
                break;
            }
            nread = read(ns, buf, BUFSIZE);
            if (nread == 0) {
                printf("Fin avec client '%s'\n", host);
                close(ns);
                break;
            } else if (nread < 0) {
                perror("read");
                close(ns);
                break;
            }
            buf[nread] = '\0';
            printf("Message recu '%s'\n", buf);
        }
    }
}
