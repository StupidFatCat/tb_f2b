#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/un.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>

#define BUFSIZE 512

int main(int argc, char **argv) {
    int s, sfd, r;
    struct addrinfo hints;
    struct addrinfo *result, *rp;
    struct sockaddr_un addr;
    char buf[BUFSIZE], *pt;
    char* socket_path = "mysocket";
    ssize_t nread, nwrite;

    printf("Usage: %s socket_path(mysocket as default)\n", argv[0]);

    if (argc > 1) {
        socket_path = argv[1];
    }


    sfd = socket(PF_UNIX, SOCK_SEQPACKET, 0);
    if (sfd == -1) {
        perror("socket error");
        exit(EXIT_FAILURE);
    }
    memset(&addr, 0, sizeof(addr));
    addr.sun_family = PF_UNIX;

    strncpy(addr.sun_path, socket_path, sizeof(addr.sun_path) - 1);

    if (connect(sfd, (struct sockaddr*)&addr, sizeof(addr)) == -1) {
        perror("connect error");
        exit(EXIT_FAILURE);
    }

    /* Dans ce qui suit, n'oubliez pas de tester le code de retour des
     * fonctions utilisees et d'emettre un message d'erreur avec perror(),
     * puis de sortir avec exit(). */

    /* Boucle de communication */
    for (;;) {
        /* Lecture socket */
        nread = read(sfd, buf, BUFSIZE);
        if (nread == 0) {
            printf("Connexion rompue\n");
            exit(EXIT_SUCCESS);
        } else if (nread < 0) {
            perror("read");
            exit(EXIT_FAILURE);
        }
        buf[nread] = '\0';
        /* Affichage ecran du message lu sur la socket */
        printf("Message recu '%s'", buf);

        /* Lecture clavier. Si on tape <Control-D>, gets() rend NULL
         * <Control-D> symbolise la fin de fichier, ici la terminaison
         * du client */
        pt = fgets(buf, BUFSIZE, stdin);
        if (pt == NULL) {
            printf("Sortie du client\n");
            exit(EXIT_SUCCESS);
        }
        nwrite = write(sfd, buf, strlen(buf));
        if (nwrite < 0) {
            perror("write");
            exit(EXIT_FAILURE);
        }
    }
}
