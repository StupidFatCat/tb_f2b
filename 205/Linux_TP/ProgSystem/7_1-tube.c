#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

int main(int argc, char* argv[]) {
    int fd[2];
    char line_in[500];
    char line_out[500];
    size_t len = 0;

    int num;

    pipe(fd);

    pid_t pid = fork();

    switch(pid) {
        case 0:
            close(fd[1]);

            dup2(fd[0], STDIN_FILENO);
            close(fd[0]);


            while (read(STDIN_FILENO, line_out, 500) != 0) {
                printf("receive from father: %s\n", line_out);
            }
            break;
        default:
            close(fd[0]);

            dup2(fd[1], STDOUT_FILENO);
            close(fd[1]);

            while (fgets(line_in, sizeof(line_in), stdin)) {
                write(STDOUT_FILENO, line_in, 500);
            }
            break;
    }

    return 0;
}
