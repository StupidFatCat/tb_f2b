/*
 * Auteur(s):
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#define TABSIZE 512

int main() {
    int pid, longueur;
    char tab[TABSIZE], *s;
    char mypath[TABSIZE];
    int i;

    for (;;) {
        fputs("petit_shell...> ", stdout);  /* Affichage d'un prompt */
        s = fgets(tab, TABSIZE, stdin);

        if (s == NULL) {
            fprintf(stderr, "Fin du Shell\n");
            exit(EXIT_SUCCESS);
        }

        longueur = strlen(s);
        tab[longueur - 1] = '\0';

        for (i = longueur - 1; i >= 0; --i) {
            if (tab[i] == '/') {
                break;
            }
        }

        strncpy(mypath, tab, i+1);
        mypath[i+1] = '\0';
        printf("mypath : %s\n", mypath);
        printf("exe : %s\n", &tab[i+1]);

        pid = fork();
        printf("pid : %d\n", pid);

        switch(pid) {
            case 0:
                //execlp(tab, &tab[i+1], NULL);
                execlp(tab, tab, NULL);
                printf("%s\n", tab);
                exit(0);
                break;
            default:
                wait(NULL);
                break;
        }
        /* Actions:
         *
         * Si dans pere alors
         *   wait(NULL);
         * sinon alors
         *   execution de la commande recuperee dans tab;
         *   message d'erreur: fprintf(stderr, "Erreur dans le exec\n")
         * fin si
         */

    }
}
