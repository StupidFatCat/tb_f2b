/*
 * Auteur(s):
 */
// compile with '-lcrypt'
#include <sys/time.h>
#include <stdio.h>
#include <termios.h>
#include <stdlib.h>
//#define _XOPEN_SOURCE /* Voir le man 3 crypt */
#define _GNU_SOURCE
#include <unistd.h>
#include <crypt.h>


#define BUFSIZE 64
char *c_key =
"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789./";

char getkey() {
    long nb;
    struct timeval tp;
    struct timezone tz;

    gettimeofday(&tp, &tz);
    srandom(tp.tv_usec);
    /* ?????????????????????????? */
    return c_key[random() % 64];

}

int setsilent(struct termios *initial_term) {
    int r;
    struct termios term;

    tcgetattr(STDIN_FILENO, &term);
    *initial_term = term;

    term.c_lflag &= ~(ECHO | ECHONL | ICANON | IEXTEN | ISIG);

    tcsetattr(STDIN_FILENO, TCSAFLUSH, &term);
    /* ??????????????????????????????? */
    return 0;
}

int restaure_term(struct termios *initial_term) {

    tcsetattr(STDIN_FILENO, TCSAFLUSH, initial_term);
    return 0;
}

/*  a utiliser au 8.4 */
char *get_pass() {
    static char buf[BUFSIZE];
    char c;
    int i = 0;

    while ((c = getchar()) != '\n') {
        buf[i++] = c;
        putchar('*');
    }

    buf[i] = '\0';
    putchar('\n');
    return buf;
}


int main() {
    int r;
    char buf[BUFSIZE], *s, *pwd;
    struct termios initial_term;
    char key[3];

    key[0] = getkey();
    key[1] = getkey();
    key[2] = '\0';
    printf("key = %s\n", key);

    printf("Tapez votre mot de passe : ");
    setsilent(&initial_term);
    //s = fgets(buf, BUFSIZE, stdin);
    s = get_pass();
    printf("%s\n", s);
    pwd = (char*) crypt(s, key);

    printf("Mot de passe chiffre : %s\n", pwd);

    restaure_term(&initial_term);
    exit(EXIT_SUCCESS);
}
