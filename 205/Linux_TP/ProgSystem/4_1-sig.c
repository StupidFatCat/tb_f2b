/*
 * Auteur(s):
 */

#include <signal.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
int num_msg = 1;
void (*sig_avant)(int);     /* pour la question 4.3 */

void hdl_sys1(int n) {
    printf("hdl_sys1: Signal recu: %d\n", n);

}

void message1(int n) {
    printf("message 1\n");
}

void message2(int n) {
    printf("message 2\n");
}

void change_message(int n) {
    printf("change message\n");
    sig_avant = signal(SIGINT, sig_avant);
}

void travail() {
    /* Je travaille tres intensement !    */
    /* Ne cherchez pas a comprendre ;-) */
    /* Il n'y a rien a modifier ici     */
    const char msg[] = "-\\|/";
    const int sz = strlen(msg);
    int i = 0;

    for (;;) {
        write(STDOUT_FILENO, "\r", 1);
        usleep(100000);
        write(STDOUT_FILENO, " => ", 4);
        write(STDOUT_FILENO, &msg[i++], 1);
        if (i == sz) i = 0;
    }
}
void travail() __attribute__((noreturn));
/* Petit raffinement pour le compilateur: cette fonction ne termine pas */


int main() {
    printf("PID: %d\n", getpid());

    //sig_avant = signal(SIGILL, hdl_sys1);
    signal(SIGINT, message1);
    sig_avant = message2;
    signal(SIGQUIT, change_message);

    travail();
}
