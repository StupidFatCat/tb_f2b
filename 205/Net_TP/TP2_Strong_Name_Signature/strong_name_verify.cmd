SET SN_EXE="C:\Program Files (x86)\Microsoft SDKs\Windows\v10.0A\bin\NETFX 4.6.1 Tools\sn.exe"
SET OUTPUT_FILE="hashcodes.txt"

del %OUTPUT_FILE%
CALL :strong_name_verify "libballon.dll" 1>>%OUTPUT_FILE%
call :print_line 1>>%OUTPUT_FILE%
CALL :strong_name_verify "libpremier.dll" 1>>%OUTPUT_FILE%
call :print_line 1>>%OUTPUT_FILE%
CALL :strong_name_verify "ThreadManager.exe" 1>>%OUTPUT_FILE%
call :print_line 1>>%OUTPUT_FILE%

PAUSE

:print_line
@echo ________________________________________________________________________
@EXIT /B 0

:strong_name_verify
@%SN_EXE% -vf %~1 
@%SN_EXE% -Tp %~1 
@EXIT /B 0
