﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Input;
using TP1.Commands;

namespace TP1.ViewModels
{
    internal class ProcessManagerViewModel : INotifyPropertyChanged
    {
        public ObservableCollection<Process> ProcessList { get; private set; }
        public int BallonNum { get { return ProcessList.Count(t => t.ProcessName == "Ballon"); } }
        public int PremierNum { get { return ProcessList.Count(t => t.ProcessName == "Premier"); } }

        public const int LimitedProcessNum = 5;

        public event EventHandler OnClosedBallonHandler;

        public event EventHandler OnClosedPremierHandler;

        #region ICommand RunBallonCommand

        public ICommand RunBallonCommand { get; private set; }
        public bool CanRunBallon { get { return CanRunProcess("Ballon"); } }

        internal void RunBallon()
        {
            var psi = new ProcessStartInfo("Ballon.exe");
            var ballon = Process.Start(psi);
            ballon.EnableRaisingEvents = true;
            ballon.Exited += OnClosedBallonHandler;
            ProcessList.Add(ballon);
        }

        #endregion ICommand RunBallonCommand

        #region ICommand RunPremierCommand

        public ICommand RunPremierCommand { get; private set; }
        public bool CanRunPremier { get { return CanRunProcess("Premier"); } }

        internal void RunPremier()
        {
            var psi = new ProcessStartInfo("Premier.exe");
            var premier = Process.Start(psi);
            premier.EnableRaisingEvents = true;
            premier.Exited += OnClosedPremierHandler;
            ProcessList.Add(premier);
        }

        #endregion ICommand RunPremierCommand

        private bool CanRunProcess(string name)
        {
            return ProcessList
                .Where(t => t.ProcessName == name)
                .Count() < LimitedProcessNum;
        }

        #region ICommand EndLastBallonCommand

        public ICommand EndLastBallonCommand { get; private set; }
        public bool CanEndLastBallon { get { return ProcessList.Any(t => t.ProcessName == "Ballon"); } }

        internal void EndLastBallon()
        {
            var ballon = ProcessList.Last(t => t.ProcessName == "Ballon");
            EndBallon(ballon);
        }

        #endregion ICommand EndLastBallonCommand

        #region ICommand EndLastPremierCommand

        public ICommand EndLastPremierCommand { get; private set; }
        public bool CanEndLastPremier { get { return ProcessList.Any(t => t.ProcessName == "Premier"); } }

        internal void EndLastPremier()
        {
            var premier = ProcessList.Last(t => t.ProcessName == "Premier");
            EndPremier(premier);
        }

        #endregion ICommand EndLastPremierCommand

        private void EndBallon(Process ballon)
        {
            ballon.Kill();
            OnClosedBallonHandler?.Invoke(ballon, null);
        }

        private void EndPremier(Process premier)
        {
            premier.Kill();
            OnClosedPremierHandler?.Invoke(premier, null);
        }

        private void EndProcess(Process process)
        {
            if (process.ProcessName == "Ballon")
            {
                EndBallon(process);
            }
            else
            {
                EndPremier(process);
            }
        }

        #region ICommand EndLastProcessCommand

        public ICommand EndLastProcessCommand { get; private set; }
        public bool CanEndLastProcess { get { return ProcessList.Any(); } }

        internal void EndLastProcess()
        {
            var process = ProcessList.Last();
            if (process.ProcessName == "Ballon")
            {
                EndBallon(process);
            }
            else
            {
                EndPremier(process);
            }
        }

        #endregion ICommand EndLastProcessCommand

        #region ICommand EndAllProcessesCommand

        public ICommand EndAllProcessesCommand { get; private set; }
        public bool CanEndAllProcesses { get { return ProcessList.Any(); } }

        internal void EndAllProcesses()
        {
            foreach (var process in ProcessList)
                EndProcess(process);
        }

        #endregion ICommand EndAllProcessesCommand

        #region ICommand QuitCommand

        public ICommand QuitCommand { get; private set; }
        public bool CanQuit { get { return true; } }

        internal void Quit()
        {
            if (CanEndAllProcesses)
            {
                EndAllProcesses();
            }
            Environment.Exit(1);
        }

        #endregion ICommand QuitCommand

        private void RemoveProcess(Process Process)
        {
            App.Current.Dispatcher.BeginInvoke(new Action(() => ProcessList.Remove(Process)));
        }

        private void InitCommands()
        {
            RunBallonCommand = new RunBallonCommand(this);
            RunPremierCommand = new RunPremierCommand(this);
            EndLastBallonCommand = new EndLastBallonCommand(this);
            EndLastPremierCommand = new EndLastPremierCommand(this);
            EndLastProcessCommand = new EndLastProcessCommand(this);
            EndAllProcessesCommand = new EndAllProcessesCommand(this);
            QuitCommand = new QuitCommand(this);
        }

        public ProcessManagerViewModel()
        {
            InitCommands();
            ProcessList = new ObservableCollection<Process>();
            ProcessList.CollectionChanged += (s, e) => OnPropertyChanged("BallonNum");
            ProcessList.CollectionChanged += (s, e) => OnPropertyChanged("PremierNum");
            OnClosedBallonHandler += (s, e) => RemoveProcess(s as Process);
            OnClosedPremierHandler += (s, e) => RemoveProcess(s as Process);
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion INotifyPropertyChanged Members
    }
}