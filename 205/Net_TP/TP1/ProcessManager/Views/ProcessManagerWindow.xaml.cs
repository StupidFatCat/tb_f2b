﻿using System.Windows;
using TP1.ViewModels;

namespace TP1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class ProcessManagerWindow : Window
    {
        public ProcessManagerWindow()
        {
            InitializeComponent();
            var viewModel = new ProcessManagerViewModel();
            DataContext = viewModel;
        }
    }
}