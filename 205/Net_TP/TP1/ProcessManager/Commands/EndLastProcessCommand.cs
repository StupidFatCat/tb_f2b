﻿using System;
using System.Windows.Input;
using TP1.ViewModels;

namespace TP1.Commands
{
    internal class EndLastProcessCommand : ICommand
    {
        private ProcessManagerViewModel viewModel;

        public EndLastProcessCommand(ProcessManagerViewModel viewModel)
        {
            this.viewModel = viewModel;
        }

        #region ICommand Members

        event EventHandler ICommand.CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        bool ICommand.CanExecute(object parameter)
        {
            return viewModel.CanEndLastProcess;
        }

        void ICommand.Execute(object parameter)
        {
            viewModel.EndLastProcess();
        }

        #endregion ICommand Members
    }
}