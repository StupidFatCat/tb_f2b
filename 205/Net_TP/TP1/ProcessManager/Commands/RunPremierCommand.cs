﻿using System;
using System.Windows.Input;
using TP1.ViewModels;

namespace TP1.Commands
{
    internal class RunPremierCommand : ICommand
    {
        private ProcessManagerViewModel viewModel;

        public RunPremierCommand(ProcessManagerViewModel viewModel)
        {
            this.viewModel = viewModel;
        }

        #region ICommand Members

        event EventHandler ICommand.CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        bool ICommand.CanExecute(object parameter)
        {
            return viewModel.CanRunPremier;
        }

        void ICommand.Execute(object parameter)
        {
            viewModel.RunPremier();
        }

        #endregion ICommand Members
    }
}