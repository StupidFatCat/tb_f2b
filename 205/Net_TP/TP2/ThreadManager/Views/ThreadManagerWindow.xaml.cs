﻿using System.Windows;
using TP2.ViewModels;

namespace TP2
{
    /// <summary>
    /// Interaction logic for ThreadManagerWindow.xaml
    /// </summary>
    public partial class ThreadManagerWindow : Window
    {
        public ThreadManagerWindow()
        {
            InitializeComponent();
            var viewModel = new ThreadManagerViewModel();
            DataContext = viewModel;
        }
    }
}