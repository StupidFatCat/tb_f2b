﻿using LibBallon;
using Premier;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using TP2.Commands;

namespace TP2.ViewModels
{
    internal class ThreadManagerViewModel : INotifyPropertyChanged
    {
        public ObservableCollection<Thread> ThreadList { get; private set; }
        public int BallonNum { get { return ThreadList.Count(t => t.Name == "Ballon"); } }
        public int PremierNum { get { return ThreadList.Count(t => t.Name == "Premier"); } }
        public string PauseOrResume { get { return !IsPaused ? "Pause" : "Resume"; } }
        public bool IsPaused { get; private set; } = false;

        public const int LimitedThreadNum = 5;

        public event EventHandler OnClosedBallonHandler;

        public event EventHandler OnClosedPremierHandler;

        #region ICommand RunBallonCommand

        public ICommand RunBallonCommand { get; private set; }
        public bool CanRunBallon { get { return CanRunThread("Ballon"); } }

        internal void RunBallon()
        {
            var ballon = new Thread(() =>
            {
                var b = new Ballon();
                b.FormClosed += (s, e) => OnClosedBallonHandler?.Invoke(Thread.CurrentThread, null);
                b.Go();
            });
            ballon.Name = "Ballon";
            ThreadList.Add(ballon);
            ballon.Start();
        }

        #endregion ICommand RunBallonCommand

        #region ICommand RunPremierCommand

        public ICommand RunPremierCommand { get; private set; }
        public bool CanRunPremier { get { return CanRunThread("Premier"); } }

        internal void RunPremier()
        {
            var count = ThreadList.Count(t => t.Name == "Premier");
            var premier = new Thread(() => NombrePremier.ThreadFunction(count));
            premier.Name = "Premier";
            ThreadList.Add(premier);
            premier.Start();
        }

        #endregion ICommand RunPremierCommand

        private bool CanRunThread(string type)
        {
            if (IsPaused)
                return false;
            return ThreadList
                .Where(t => t.Name == type)
                .Count() < LimitedThreadNum;
        }

        #region ICommand PauseResumeCommand

        public ICommand PauseResumeCommand { get; private set; }
        public bool CanPauseResume { get { return ThreadList.Any(); } }

        internal void PauseResume()
        {
            if (!IsPaused)
            {
                PauseAllThreads();
            }
            else
            {
                ResumeAllThreads();
            }
        }

        private void PauseAllThreads()
        {
            foreach (var thread in ThreadList)
            {
                thread.Suspend();
            }
            IsPaused = true;
            OnPropertyChanged("PauseOrResume");
        }

        private void ResumeAllThreads()
        {
            foreach (var thead in ThreadList)
            {
                thead.Resume();
            }
            IsPaused = false;
            OnPropertyChanged("PauseOrResume");
        }

        #endregion ICommand PauseResumeCommand

        #region ICommand EndLastBallonCommand

        public ICommand EndLastBallonCommand { get; private set; }
        public bool CanEndLastBallon { get { return ThreadList.Any(t => t.Name == "Ballon"); } }

        internal void EndLastBallon()
        {
            if (IsPaused)
            {
                MessageBox.Show("Need to resume before end threads.");
            }
            else
            {
                var ballon = ThreadList.Last(t => t.Name == "Ballon");
                EndBallon(ballon);
            }
        }

        #endregion ICommand EndLastBallonCommand

        #region ICommand EndLastPremierCommand

        public ICommand EndLastPremierCommand { get; private set; }
        public bool CanEndLastPremier { get { return ThreadList.Any(t => t.Name == "Premier"); } }

        internal void EndLastPremier()
        {
            if (IsPaused)
            {
                MessageBox.Show("Need to resume before end threads.");
            }
            else
            {
                var premier = ThreadList.Last(t => t.Name == "Premier");
                EndPremier(premier);
            }
        }

        #endregion ICommand EndLastPremierCommand

        private void EndBallon(Thread ballon)
        {
            ballon.Abort();
            OnClosedBallonHandler?.Invoke(ballon, null);
        }

        private void EndPremier(Thread premier)
        {
            premier.Abort();
            OnClosedPremierHandler?.Invoke(premier, null);
        }

        private void EndThread(Thread thread)
        {
            if (thread.Name == "Ballon")
            {
                EndBallon(thread);
            }
            else
            {
                EndPremier(thread);
            }
        }

        #region ICommand EndLastThreadCommand

        public ICommand EndLastThreadCommand { get; private set; }
        public bool CanEndLastThread { get { return ThreadList.Any(); } }

        internal void EndLastThread()
        {
            if (IsPaused)
            {
                MessageBox.Show("Need to resume before end threads.");
            }
            else
            {
                var thread = ThreadList.Last();
                if (thread.Name == "Ballon")
                {
                    EndBallon(thread);
                }
                else
                {
                    EndPremier(thread);
                }
            }
        }

        #endregion ICommand EndLastThreadCommand

        #region ICommand EndAllThreadsCommand

        public ICommand EndAllThreadsCommand { get; private set; }
        public bool CanEndAllThreads { get { return ThreadList.Any(); } }

        internal void EndAllThreads()
        {
            if (IsPaused)
            {
                MessageBox.Show("Need to resume before end threads.");
            }
            else
            {
                foreach (var thread in ThreadList)
                    EndThread(thread);
            }
        }

        #endregion ICommand EndAllThreadsCommand

        #region ICommand QuitCommand

        public ICommand QuitCommand { get; private set; }
        public bool CanQuit { get { return !IsPaused; } }

        internal void Quit()
        {
            if (IsPaused)
            {
                ResumeAllThreads();
            }
            if (CanEndAllThreads)
            {
                EndAllThreads();
            }
            Environment.Exit(1);
        }

        #endregion ICommand QuitCommand

        private void RemoveThread(Thread thread)
        {
            App.Current.Dispatcher.BeginInvoke(new Action(() => ThreadList.Remove(thread)));
        }

        private void InitCommands()
        {
            RunBallonCommand = new RunBallonCommand(this);
            RunPremierCommand = new RunPremierCommand(this);
            PauseResumeCommand = new PauseResumeCommand(this);
            EndLastBallonCommand = new EndLastBallonCommand(this);
            EndLastPremierCommand = new EndLastPremierCommand(this);
            EndLastThreadCommand = new EndLastThreadCommand(this);
            EndAllThreadsCommand = new EndAllThreadsCommand(this);
            QuitCommand = new QuitCommand(this);
        }

        public ThreadManagerViewModel()
        {
            InitCommands();
            ThreadList = new ObservableCollection<Thread>();
            ThreadList.CollectionChanged += (s, e) => OnPropertyChanged("BallonNum");
            ThreadList.CollectionChanged += (s, e) => OnPropertyChanged("PremierNum");
            Console.SetWindowSize(20, 30);
            OnClosedBallonHandler += (s, e) => RemoveThread(s as Thread);
            OnClosedPremierHandler += (s, e) => RemoveThread(s as Thread);
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion INotifyPropertyChanged Members
    }
}