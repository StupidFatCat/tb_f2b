﻿using System;
using System.Windows.Input;
using TP2.ViewModels;

namespace TP2.Commands
{
    internal class EndLastBallonCommand : ICommand
    {
        private ThreadManagerViewModel viewModel;

        public EndLastBallonCommand(ThreadManagerViewModel viewModel)
        {
            this.viewModel = viewModel;
        }

        #region ICommand Members

        event EventHandler ICommand.CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        bool ICommand.CanExecute(object parameter)
        {
            return viewModel.CanEndLastBallon;
        }

        void ICommand.Execute(object parameter)
        {
            viewModel.EndLastBallon();
        }

        #endregion ICommand Members
    }
}