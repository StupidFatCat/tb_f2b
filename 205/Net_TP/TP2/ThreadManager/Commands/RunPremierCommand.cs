﻿using System;
using System.Windows.Input;
using TP2.ViewModels;

namespace TP2.Commands
{
    internal class RunPremierCommand : ICommand
    {
        private ThreadManagerViewModel viewModel;

        public RunPremierCommand(ThreadManagerViewModel viewModel)
        {
            this.viewModel = viewModel;
        }

        #region ICommand Members

        event EventHandler ICommand.CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        bool ICommand.CanExecute(object parameter)
        {
            return viewModel.CanRunPremier;
        }

        void ICommand.Execute(object parameter)
        {
            viewModel.RunPremier();
        }

        #endregion ICommand Members
    }
}