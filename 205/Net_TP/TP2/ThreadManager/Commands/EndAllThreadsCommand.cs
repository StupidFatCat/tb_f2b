﻿using System;
using System.Windows.Input;
using TP2.ViewModels;

namespace TP2.Commands
{
    internal class EndAllThreadsCommand : ICommand
    {
        private ThreadManagerViewModel viewModel;

        public EndAllThreadsCommand(ThreadManagerViewModel viewModel)
        {
            this.viewModel = viewModel;
        }

        #region ICommand Members

        event EventHandler ICommand.CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        bool ICommand.CanExecute(object parameter)
        {
            return viewModel.CanEndAllThreads;
        }

        void ICommand.Execute(object parameter)
        {
            viewModel.EndAllThreads();
        }

        #endregion ICommand Members
    }
}