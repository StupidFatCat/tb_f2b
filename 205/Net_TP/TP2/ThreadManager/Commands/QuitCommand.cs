﻿using System;
using System.Windows.Input;
using TP2.ViewModels;

namespace TP2.Commands
{
    internal class QuitCommand : ICommand
    {
        private ThreadManagerViewModel viewModel;

        public QuitCommand(ThreadManagerViewModel viewModel)
        {
            this.viewModel = viewModel;
        }

        #region ICommand Members

        event EventHandler ICommand.CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        bool ICommand.CanExecute(object parameter)
        {
            return viewModel.CanQuit;
        }

        void ICommand.Execute(object parameter)
        {
            viewModel.Quit();
        }

        #endregion ICommand Members
    }
}