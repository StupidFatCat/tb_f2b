﻿using System;
using System.Windows.Input;
using TP2.ViewModels;

namespace TP2.Commands
{
    internal class RunBallonCommand : ICommand
    {
        private ThreadManagerViewModel viewModel;

        public RunBallonCommand(ThreadManagerViewModel viewModel)
        {
            this.viewModel = viewModel;
        }

        #region ICommand Members

        event EventHandler ICommand.CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        bool ICommand.CanExecute(object parameter)
        {
            return viewModel.CanRunBallon;
        }

        void ICommand.Execute(object parameter)
        {
            viewModel.RunBallon();
        }

        #endregion ICommand Members
    }
}