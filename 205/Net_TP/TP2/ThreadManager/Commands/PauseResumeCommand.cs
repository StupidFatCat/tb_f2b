﻿using System;
using System.Windows.Input;
using TP2.ViewModels;

namespace TP2.Commands
{
    internal class PauseResumeCommand : ICommand
    {
        private ThreadManagerViewModel viewModel;

        public PauseResumeCommand(ThreadManagerViewModel viewModel)
        {
            this.viewModel = viewModel;
        }

        #region ICommand Members

        event EventHandler ICommand.CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        bool ICommand.CanExecute(object parameter)
        {
            return viewModel.CanPauseResume;
        }

        void ICommand.Execute(object parameter)
        {
            viewModel.PauseResume();
        }

        #endregion ICommand Members
    }
}