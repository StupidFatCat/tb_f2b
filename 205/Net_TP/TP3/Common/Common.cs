﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatRoomCommon
{
    public delegate void MessageReceivedHandler(Message message);

    public delegate void UpdateUserListHandler(ObservableCollection<string> userlist);

    public interface IRemoteChain
    {
        event MessageReceivedHandler MessageReceived;
        event UpdateUserListHandler UpdateUserList;

        void PublishMessage(Message message);

        bool ClientLogin(string pseudo);
        void ClientLogout(string pseudo);

        void UpdateUsers();
    }

    [Serializable]
    public class Message
    {
        public string Content { get; private set; }
        public string UserName { get; private set; }

        public Message(string username, string content)
        {
            UserName = username;
            Content = content;
        }
    }

    public class MessageProxy : MarshalByRefObject
    {
        public event MessageReceivedHandler MessageReceived;
        public UpdateUserListHandler UpdateUserList;

        public void MessageReceivedCallback(Message message)
        {
            if (MessageReceived != null)
            {
                MessageReceived(message);
            }
        }

        public void UpdateUserListCallback(ObservableCollection<string> userlist)
        {
            if (UpdateUserList != null)
            {
                UpdateUserList(userlist);
            }
        }




        public override object InitializeLifetimeService()
        {
            return null;
            //return base.InitializeLifetimeService();
        }

    }
}
