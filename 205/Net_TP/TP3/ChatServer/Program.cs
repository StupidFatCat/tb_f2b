﻿using System;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;

using ChatRoomCommon;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace remoteServer
{
	/// <summary>
	/// Description résumée de demarreServeur.
	/// </summary>
	public class Server : MarshalByRefObject, IRemoteChain
	{
        private ObservableCollection<string> userlist = new ObservableCollection<string>();

		static void Main()
		{
            // Création d'un nouveau canal pour le transfert des données via un port 
            Hashtable properties = new Hashtable();
            properties["name"] = "ChatServer";
            properties["port"] = 23333;
            BinaryServerFormatterSinkProvider serverProvider = new BinaryServerFormatterSinkProvider();
            
            serverProvider.TypeFilterLevel = System.Runtime.Serialization.Formatters.TypeFilterLevel.Full;

			var channel = new TcpServerChannel(properties, serverProvider);

			// Le canal ainsi défini doit être Enregistré dans l'annuaire
			ChannelServices.RegisterChannel(channel, false);

			// Démarrage du serveur en écoute sur objet en mode Singleton
			// Publication du type avec l'URI et son mode 
			RemotingConfiguration.RegisterWellKnownServiceType(
				typeof(Server), "Server",  WellKnownObjectMode.Singleton);

			Console.WriteLine("Le serveur est bien démarré");
			// pour garder la main sur la console
			Console.ReadLine();	
		}

		// Pour laisser le serveur fonctionner sans time out
		public override object  InitializeLifetimeService()
		{
			return null;
		}


        #region IRemoteChain Members

        public event MessageReceivedHandler MessageReceived;
        public event UpdateUserListHandler UpdateUserList;

		public void PublishMessage(Message message)
		{
            MessageReceived(message);
		}

        public bool ClientLogin(string pseudo)
        {
            if (!string.IsNullOrWhiteSpace(pseudo))
            {
                if (userlist.Contains(pseudo))
                {
                    return false;
                }
                else
                {
                    userlist.Add(pseudo);
                    return true;
                }
            }
            return false;
        }

        public void ClientLogout(string pseudo)
        {
            userlist.Remove(pseudo);
            UpdateUsers();
        }

        public void UpdateUsers()
        {
            UpdateUserList(userlist);
        }

		#endregion
	}
}
