﻿using System.ComponentModel;

namespace TP3.Models
{
    internal class MessageToSend : INotifyPropertyChanged
    {
        private string content;
        private string username;

        public string Content
        {
            get
            {
                return content;
            }
            set
            {
                content = value;
                OnPropertyChanged("Content");
            }
        }

        public string UserName
        {
            get
            {
                return username;
            }
            private set
            {
                username = value;
                OnPropertyChanged("UserName");
            }
        }

        public MessageToSend(string username)
        {
            UserName = username;
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion INotifyPropertyChanged Members
    }
}