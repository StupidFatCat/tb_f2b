﻿using System.ComponentModel;

namespace TP3.Models
{
    internal class Login : INotifyPropertyChanged
    {
        private string pseudo;
        private string serverUrl;

        public string Pseudo
        {
            get { return pseudo; }
            set
            {
                pseudo = value;
                OnPropertyChanged("Pseudo");
            }
        }

        public string ServerUrl
        {
            get { return serverUrl; }
            set
            {
                serverUrl = value;
                OnPropertyChanged("ServerUrl");
            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion INotifyPropertyChanged Members
    }
}