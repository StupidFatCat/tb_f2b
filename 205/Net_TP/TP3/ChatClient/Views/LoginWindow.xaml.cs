﻿using System.Windows;
using TP3.ViewModels;

namespace TP3.Views
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        public LoginWindow()
        {
            InitializeComponent();
            var view = new LoginViewModel();
            view.HideView += (s, e) => this.Hide();
            view.ShowView += (s, e) => this.Show();
            DataContext = view;
        }
    }
}