﻿using ChatRoomCommon;
using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;
using TP3.Commands;
using TP3.Models;

namespace TP3.ViewModels
{
    internal class ChatRoomViewModel
    {
        private MessageProxy messageProxy;
        private IRemoteChain iRemote;

        public ObservableCollection<string> UserList { get; private set; }
        public ObservableCollection<Message> MessageList { get; private set; }
        public MessageToSend MessageToSend { get; private set; }

        public ICommand SendMessageCommand { get; private set; }

        public bool CanSendMessage
        {
            get
            {
                if (MessageToSend == null)
                {
                    return false;
                }
                return !string.IsNullOrWhiteSpace(MessageToSend.Content);
            }
        }

        public ChatRoomViewModel(IRemoteChain iRemote, Login login)
        {
            this.iRemote = iRemote;
            UserList = new ObservableCollection<string>();
            MessageList = new ObservableCollection<Message>();
            MessageToSend = new MessageToSend(login.Pseudo);
            SendMessageCommand = new ClientSendMessageCommand(this);

            InitHandlers();
            iRemote.UpdateUsers();
        }

        private void InitHandlers()
        {
            messageProxy = new MessageProxy();
            messageProxy.MessageReceived += AddMessage;
            messageProxy.UpdateUserList = UpdateUserList;

            iRemote.MessageReceived += messageProxy.MessageReceivedCallback;
            iRemote.UpdateUserList += messageProxy.UpdateUserListCallback;
        }

        internal void SendMessage()
        {
            var message = new Message(MessageToSend.UserName, MessageToSend.Content);
            iRemote.PublishMessage(message);
            MessageToSend.Content = "";
        }

        private void RemoveHandlers()
        {
            iRemote.MessageReceived -= messageProxy.MessageReceivedCallback;
            iRemote.UpdateUserList -= messageProxy.UpdateUserListCallback;
        }

        private void UpdateUserList(ObservableCollection<string> userlist)
        {
            App.Current.Dispatcher.BeginInvoke(
                new Action(() =>
                {
                    UserList.Clear();
                    foreach (var user in userlist)
                    {
                        UserList.Add(user);
                    }
                }),
                System.Windows.Threading.DispatcherPriority.DataBind,
                null);
        }

        private void AddMessage(Message message)
        {
            Application.Current.Dispatcher.BeginInvoke(
                new Action(() => MessageList.Add(message)),
                System.Windows.Threading.DispatcherPriority.DataBind,
                null);
        }
    }
}