﻿using ChatRoomCommon;
using System;
using System.Collections;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Windows;
using System.Windows.Input;
using TP3.Commands;
using TP3.Models;
using TP3.Views;

namespace TP3.ViewModels
{
    internal class LoginViewModel
    {
        private TcpChannel channel;
        private IRemoteChain iRemote;
        private BinaryClientFormatterSinkProvider clientProvider;
        private BinaryServerFormatterSinkProvider serverProvider;
        private Hashtable properties;

        private ChatRoomViewModel chatRoom;

        public event EventHandler HideView;

        public event EventHandler ShowView;

        public Login Login { get; private set; }

        public ICommand ConnectCommand { get; private set; }

        public bool CanConnect
        {
            get
            {
                if (Login == null)
                    return false;
                return !string.IsNullOrWhiteSpace(Login.Pseudo) && !string.IsNullOrWhiteSpace(Login.ServerUrl);
            }
        }

        public LoginViewModel()
        {
            Login = new Login();
            Login.ServerUrl = "tcp://localhost:23333/Server";
            ConnectCommand = new ClientConnectCommand(this);

            // Unregister channel when closing chat room
            ShowView += (s, e) => ChannelServices.UnregisterChannel(channel);
        }

        private void InitChannel()
        {
            clientProvider = new BinaryClientFormatterSinkProvider();
            serverProvider = new BinaryServerFormatterSinkProvider();
            serverProvider.TypeFilterLevel = System.Runtime.Serialization.Formatters.TypeFilterLevel.Full;
            properties = new Hashtable();
            properties["name"] = "ClientChat";
            properties["port"] = 0;

            channel = new TcpChannel(properties, clientProvider, serverProvider);

            ChannelServices.RegisterChannel(channel, false);

            iRemote = (IRemoteChain)Activator.GetObject(typeof(IRemoteChain), Login.ServerUrl);
        }

        internal void ConnectToServer()
        {
            InitChannel();
            if (iRemote == null)
            {
                ChannelServices.UnregisterChannel(channel);
                MessageBox.Show("Cannot connect to server.");

                // login success
            }
            else if (iRemote.ClientLogin(Login.Pseudo))
            {
                chatRoom = new ChatRoomViewModel(iRemote, Login);
                var view = new ChatRoomWindow();
                view.DataContext = chatRoom;
                view.Closed += (s, e) => iRemote.ClientLogout(Login.Pseudo);
                view.Closed += ShowView;
                HideView?.Invoke(this, null);
                view.ShowDialog();
            }
            else
            {
                ChannelServices.UnregisterChannel(channel);
                MessageBox.Show("Cannot login to server. Please Check your input");
            }
        }
    }
}