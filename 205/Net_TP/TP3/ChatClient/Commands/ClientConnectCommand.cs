﻿using System;
using System.Windows.Input;
using TP3.ViewModels;

namespace TP3.Commands
{
    internal class ClientConnectCommand : ICommand
    {
        private LoginViewModel viewModel;

        public ClientConnectCommand(LoginViewModel viewModel)
        {
            this.viewModel = viewModel;
        }

        #region ICommand Members

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            return viewModel.CanConnect;
        }

        public void Execute(object parameter)
        {
            viewModel.ConnectToServer();
        }

        #endregion ICommand Members
    }
}