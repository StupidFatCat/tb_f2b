﻿using System;
using System.Windows.Input;
using TP3.ViewModels;

namespace TP3.Commands
{
    internal class ClientSendMessageCommand : ICommand
    {
        private ChatRoomViewModel viewModel;

        public ClientSendMessageCommand(ChatRoomViewModel viewModel)
        {
            this.viewModel = viewModel;
        }

        #region ICommand Members

        event EventHandler ICommand.CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        bool ICommand.CanExecute(object parameter)
        {
            return viewModel.CanSendMessage;
        }

        void ICommand.Execute(object parameter)
        {
            viewModel.SendMessage();
        }

        #endregion ICommand Members
    }
}